import {DefaultCrudRepository, juggler} from '@loopback/repository';
import {User} from '../models';
import {MysqlDSDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class UserRepository extends DefaultCrudRepository<
  User,
  typeof User.prototype.id
> {
  constructor(
    @inject('datasources.mysqlDS') dataSource: MysqlDSDataSource,
  ) {
    super(User, dataSource);
  }
}
