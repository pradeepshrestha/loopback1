import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getFilterSchemaFor,
  getWhereSchemaFor,
  patch,
  del,
  requestBody,
  HttpErrors
} from '@loopback/rest';



import {User} from '../models';
import {UserRepository} from '../repositories';

//import {hash} from 'bcryptjs';
import * as bcryptjs from 'bcryptjs'
//import bcryptjs = require("bcryptjs")

import {promisify} from 'util';
//const hashAsync = promisify(hash);
import * as isemail from 'isemail';
import * as jwt from 'jsonwebtoken';


export class UserController {
 
  constructor(
    @repository(UserRepository)
    public userRepository : UserRepository,
  ) {}

  @post('/users/signup', {
    responses: {
      '200': {
        description: 'User model instance',
        content: {'application/json': {'x-ts-type': User}},
      },
    },
  })
  async create(@requestBody() user: User): Promise<User> {
     // Validate Password Length
     if (user.password.length < 3) {
      throw new HttpErrors.UnprocessableEntity(
        'password must be minimum 8 characters',
      );
    }
    //user.password = await hashAsync(user.password, 10);
    let plain=user.password
    user.password=bcryptjs.hashSync(user.password, 10);
  
  //  let check= bcryptjs.compareSync("plain",user.password);
  //  console.log("check",check)
      // Validate Email
      if (!isemail.validate(user.email)) {
        throw new HttpErrors.UnprocessableEntity('invalid email');
      }

        // Save & Return Result
    const savedUser = await this.userRepository.create(user);
     //delete savedUser.password;
     return savedUser;
  
  }

  
  @post('/users/login', {
    responses: {
      '200': {
        description: 'User model instance',
        content: {'application/json': {'x-ts-type': User}},
      },
    },
  })
  // Promise<User>
  async login(@requestBody() user: User):  Promise<any> {
   let db_user=await this.userRepository.find({where: {username: user.username}});
       let isMatched;
     
       if(db_user[0])
       isMatched=bcryptjs.compareSync(user.password,db_user[0].password);
    
 if(isMatched){
  
    const token = jwt.sign(user, "mysecret", {
      expiresIn: 604800 // 1 week
    });
    
       return({
      success: true,
      token:'JWT '+token,
       user
    });
  } else {
    return ({success: false, msg: 'Wrong password'});
  }
 
  
  
   
  
  }
  
  
  @get('/users/count', {
    responses: {
      '200': {
        description: 'User model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async count(
    @param.query.object('where', getWhereSchemaFor(User)) where?: Where,
  ): Promise<Count> {
    return await this.userRepository.count(where);
  }

  @get('/users', {
    responses: {
      '200': {
        description: 'Array of User model instances',
        content: {
          'application/json': {
            schema: {type: 'array', items: {'x-ts-type': User}},
          },
        },
      },
    },
  })
  async find(
    @param.query.object('filter', getFilterSchemaFor(User)) filter?: Filter,
  ): Promise<User[]> {
    return await this.userRepository.find(filter);
  }

  @patch('/users', {
    responses: {
      '200': {
        description: 'User PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async updateAll(
    @requestBody() user: User,
    @param.query.object('where', getWhereSchemaFor(User)) where?: Where,
  ): Promise<Count> {
    return await this.userRepository.updateAll(user, where);
  }
// @get('/users/{id}')
  // async findById(@param.path.string('id') id: string): Promise<User> {
  //   return this.userRepository.findById(id, {
  //     fields: {password: false},
  //   });
  // }
  @get('/users/{id}', {
    responses: {
      '200': {
        description: 'User model instance',
        content: {'application/json': {'x-ts-type': User}},
      },
    },
  })
  async findById(@param.path.number('id') id: number): Promise<User> {
    return await this.userRepository.findById(id, {
          fields: {password: false},
         });
  }

  @patch('/users/{id}', {
    responses: {
      '204': {
        description: 'User PATCH success',
      },
    },
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody() user: User,
  ): Promise<void> {
    await this.userRepository.updateById(id, user);
  }

  @del('/users/{id}', {
    responses: {
      '204': {
        description: 'User DELETE success',
      },
    },
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.userRepository.deleteById(id);
  }
}
