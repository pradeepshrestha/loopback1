import {inject} from '@loopback/core';
import {juggler} from '@loopback/repository';
import * as config from './mysql-ds.datasource.json';

export class MysqlDSDataSource extends juggler.DataSource {
  static dataSourceName = 'mysqlDS';

  constructor(
    @inject('datasources.config.mysqlDS', {optional: true})
    dsConfig: object = config,
  ) {
    super(dsConfig);
  }
}
